<?php
/**
 * Created by PhpStorm.
 * User: Harsh
 * Date: 20-05-2018
 * Time: 16:50
 */
function sub_category(){
    ?>

        <div class="col-sm-12 col-xs-12 pd_bt45">
            <div class="container">
                <div class="carousel">
<!--
                    <div class="text-center">
                        <h2>Sub Categories</h2><hr class="hr_wid">
                    </div>
-->
                  <div id="firstCar" class="owl-carousel">
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                    <div class="item pd_lft0">
                        <a href="#">
                            <div class="item_sub">
                                <div class="icon_div">
                                    <div class="icon_size"><img src="<?php echo HOME . 'images/icons/coffee-cup.png' ?>"></div>
                                </div>
                                <p class="text-center"><b>Business Cards</b></p>
                            </div>
                        </a>
                    </div>
                  </div>
                </div>
            </div>
            <script>
                $("#firstCar").owlCarousel({
                  autoplay: true,
                  lazyLoad: true,
                  loop: true,
                  autoplayTimeout:2000,
                  margin: 7,
                  responsiveClass: true,
                  autoHeight: true,
                  nav: true,
                  navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
                    dots:false,
                  responsive: {
                    0: {
                      items: 2
                    },

                    600: {
                      items: 4
                    },

                    1024: {
                      items: 5
                    },

                    1366: {
                      items: 7
                    }
                  }
                });
            </script>
        </div>
    <?php
}
?>
