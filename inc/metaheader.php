<?php
/**
 * Created by PhpStorm.
 * User: harsh
 * Date: 07-May-18
 * Time: 11:23 AM
/**
 * Created by PhpStorm.
 * User: sniizzer
 * Date: 11-06-2016
 * Time: 13:37
 */

require_once 'links.php';
require_once 'navbar.php';
require_once 'footer.php';
require_once 'slider.php';
require_once 'below_slider.php';
require_once 'prod_slider.php';
require_once 'sub_category_carousel.php';

/*require_once 'slider_menu.php';*/



$resources = array();

function meta_header()
{
    ?>
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="">
    <?php
}

function title($title)
{
    if (!empty($title) or $title != "")
        echo'<title>'.$title.'</title>';
}

function description($description)
{
    echo '<meta name="description" content="'.$description.'">';
}

function resources()
{
    global $resources;
    ?>
    <title> Customized Gift Shop </title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo HOME . 'css/bootstrap.min.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/font-awesome.min.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/prettyPhoto.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/price-range.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/animate.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/main.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'fa_css/fontawesome.min.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'fa_css/fontawesome-all.min.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/responsive.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/new_style.css' ?>" rel="stylesheet">
    <link href="<?php echo HOME . 'css/owl.carousel.min.css' ?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.theme.default.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo HOME . 'images/ico/favicon.ico' ?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo HOME . 'images/ico/apple-touch-icon-144-precomposed.png' ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo HOME . 'images/ico/apple-touch-icon-114-precomposed.png' ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo HOME . 'images/ico/apple-touch-icon-72-precomposed.png' ?>">
    <link rel="apple-touch-icon-precomposed" href="<?php echo HOME . 'images/ico/apple-touch-icon-57-precomposed.png' ?>">




    <script src="<?php echo HOME . 'js/jquery.js' ?>"></script>
    <script src="<?php echo HOME . 'js/bootstrap.min.js' ?>"></script>
    <script src="<?php echo HOME . 'js/jquery.scrollUp.min.js' ?>"></script>
    <script src="<?php echo HOME . 'js/price-range.js' ?>"></script>
    <script src="<?php echo HOME . 'js/jquery.prettyPhoto.js' ?>"></script>
    <script src="<?php echo HOME . 'js/main.js' ?>"></script>
    <script src="<?php echo HOME . 'js/owl.carousel.min.js' ?>"></script>
<?php
//foreach ($resources as $res) {
//    echo $res;
//}
//?>
    </head>
    <?php
}

function add_resource($resource)
{
    global $resources;
    array_push($resources, $resource);
}

function meta($title = "")
{
    meta_header();
    title($title);
    resources();
}

?>