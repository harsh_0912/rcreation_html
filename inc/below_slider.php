<?php
/**
 * Created by PhpStorm.
 * User: Harsh
 * Date: 20-05-2018
 * Time: 16:50
 */
function below_slider(){
    ?>
        <div class="col-sm-12 col-xs-12 pd_bt45">
            <div class="container bg_box">
                <div class="row dis_fl">
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="fas fa-address-card"></i></h4>
                        </div>
                        <p class="text-center"><b>Business Cards</b></p>
                    </div>
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="fas fa-coffee"></i></h4>
                        </div>
                        <p class="text-center"><b>Coffee Mugs</b></p>
                    </div>
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="fas fa-tshirt"></i></h4>
                        </div>
                        <p class="text-center"><b>T-Shirts</b></p>
                    </div>
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="fas fa-mobile-alt"></i></h4>
                        </div>
                        <p class="text-center"><b>Mobile Covers</b></p>
                    </div>
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="fas fa-laptop"></i></h4>
                        </div>
                        <p class="text-center"><b>Laptop Skins</b></p>
                    </div>
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="far fa-id-badge"></i></h4>
                        </div>
                        <p class="text-center"><b>ID Cards</b></p>
                    </div>
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="fab fa-linux"></i></h4>
                        </div>
                        <p class="text-center"><b>Gifts For Kids</b></p>
                    </div>
                    <div class="icon_1 col-xs-6">
                        <div class="icon_div">
                            <h4 class="icon_size"><i class="fas fa-headphones"></i></h4>
                        </div>
                        <p class="text-center"><b>Headphones</b></p>
                    </div>
                </div>
            </div>
        </div>
    <?php
}
?>
