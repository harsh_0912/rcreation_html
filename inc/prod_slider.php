<?php
/**
 * Created by PhpStorm.
 * User: Harsh
 * Date: 22-05-2018
 * Time: 14:46
 */
function prod_slider(){
    ?>
        <div class="col-sm-12 col-xs-12 pd_bt45">
            <div class="container" style="background:#d4d4d442;">
                <div class="row">
                    <div class="col-sm-3 pd_tp35">
                        <img class="img_car" src="<?php echo HOME . 'images/home/img_bestseller.jpg' ?>">
                    </div>
                    <div class="col-sm-9">
                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <div class="col-sm-4 col-xs-12 nopadh">
                                            <div class="product-image-wrapper marg_bt0 marg_bt0">
                                                <div class="single-products pad_1015">
                                                    <div class="productinfo prod_card text-center">
                                                        <img src="<?php echo HOME . 'images/home/recommend1.jpg' ?>" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <div class="row">
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-eye"></i>View Details</a>
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12 nopadh">
                                            <div class="product-image-wrapper marg_bt0">
                                                <div class="single-products pad_1015">
                                                    <div class="productinfo prod_card text-center">
                                                        <img src="<?php echo HOME . 'images/home/recommend2.jpg' ?>" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <div class="row">
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-eye"></i>View Details</a>
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12 nopadh">
                                            <div class="product-image-wrapper marg_bt0">
                                                <div class="single-products pad_1015">
                                                    <div class="productinfo prod_card text-center">
                                                        <img src="<?php echo HOME . 'images/home/recommend3.jpg' ?>" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <div class="row">
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-eye"></i>View Details</a>
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="item">
                                        <div class="col-sm-4 col-xs-12 nopadh">
                                            <div class="product-image-wrapper marg_bt0">
                                                <div class="single-products pad_1015">
                                                    <div class="productinfo prod_card text-center">
                                                        <img src="<?php echo HOME . 'images/home/recommend1.jpg' ?>" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <div class="row">
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-eye"></i>View Details</a>
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12 nopadh">
                                            <div class="product-image-wrapper marg_bt0">
                                                <div class="single-products pad_1015">
                                                    <div class="productinfo prod_card text-center">
                                                        <img src="<?php echo HOME . 'images/home/recommend2.jpg' ?>" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <div class="row">
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-eye"></i>View Details</a>
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-12 nopadh">
                                            <div class="product-image-wrapper marg_bt0">
                                                <div class="single-products pad_1015">
                                                    <div class="productinfo prod_card text-center">
                                                        <img src="<?php echo HOME . 'images/home/recommend2.jpg' ?>" alt="" />
                                                        <h2>$56</h2>
                                                        <p>Easy Polo Black Edition</p>
                                                        <div class="row">
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-eye"></i>View Details</a>
                                                            <a href="#" class="btn btn-default add-to-cart1"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                    </div>
                </div>
                    
                </div><!--/recommended_items-->
            </div>


    <?php } ?>
