<?php
/**
 * Created by PhpStorm.
 * User: Harsh
 * Date: 19-05-2018
 * Time: 15:28
 */
function navbar(){
    ?>


    <header id="header"><!--header-->
        <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            <ul class="nav nav-pills">
                                <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="social-icons pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->

        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="logo pull-left">
                            <a href="<?php echo HOME . 'index.php' ?>"><img src="<?php echo HOME . 'images/home/logo-black.png' ?>" style="height:60px;" alt="_logo" /></a>
                        </div>
                        <!-- <div class="btn-group pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    USA
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Canada</a></li>
                                    <li><a href="#">UK</a></li>
                                </ul>
                            </div>

                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                                    DOLLAR
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Canadian Dollar</a></li>
                                    <li><a href="#">Pound</a></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                    <div class="col-sm-5">
                    </div>
                    <div class="col-sm-5">
                        <div class="shop-menu pull-right">
                            <form id="search" autocomplete="off" action="/action_page.php">
                              <div class="autocomplete" style="width:300px;">
                                <input id="myInput" type="text" name="myCountry" placeholder="Enter Your Search">
                              </div>
                              <button type="submit"><i class="fas fa-search"></i>&nbsp;Search</button>
                            </form>
                            <script>
                                function autocomplete(inp, arr) {
                                  /*the autocomplete function takes two arguments,
                                  the text field element and an array of possible autocompleted values:*/
                                  var currentFocus;
                                  /*execute a function when someone writes in the text field:*/
                                  inp.addEventListener("input", function(e) {
                                      var a, b, i, val = this.value;
                                      /*close any already open lists of autocompleted values*/
                                      closeAllLists();
                                      if (!val) { return false;}
                                      currentFocus = -1;
                                      /*create a DIV element that will contain the items (values):*/
                                      a = document.createElement("DIV");
                                      a.setAttribute("id", this.id + "autocomplete-list");
                                      a.setAttribute("class", "autocomplete-items");
                                      /*append the DIV element as a child of the autocomplete container:*/
                                      this.parentNode.appendChild(a);
                                      /*for each item in the array...*/
                                      for (i = 0; i < arr.length; i++) {
                                        /*check if the item starts with the same letters as the text field value:*/
                                        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                          /*create a DIV element for each matching element:*/
                                          b = document.createElement("DIV");
                                          /*make the matching letters bold:*/
                                          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                          b.innerHTML += arr[i].substr(val.length);
                                          /*insert a input field that will hold the current array item's value:*/
                                          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                          /*execute a function when someone clicks on the item value (DIV element):*/
                                          b.addEventListener("click", function(e) {
                                              /*insert the value for the autocomplete text field:*/
                                              inp.value = this.getElementsByTagName("input")[0].value;
                                              /*close the list of autocompleted values,
                                              (or any other open lists of autocompleted values:*/
                                              closeAllLists();
                                          });
                                          a.appendChild(b);
                                        }
                                      }
                                  });
                                  /*execute a function presses a key on the keyboard:*/
                                  inp.addEventListener("keydown", function(e) {
                                      var x = document.getElementById(this.id + "autocomplete-list");
                                      if (x) x = x.getElementsByTagName("div");
                                      if (e.keyCode == 40) {
                                        /*If the arrow DOWN key is pressed,
                                        increase the currentFocus variable:*/
                                        currentFocus++;
                                        /*and and make the current item more visible:*/
                                        addActive(x);
                                      } else if (e.keyCode == 38) { //up
                                        /*If the arrow UP key is pressed,
                                        decrease the currentFocus variable:*/
                                        currentFocus--;
                                        /*and and make the current item more visible:*/
                                        addActive(x);
                                      } else if (e.keyCode == 13) {
                                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                                        e.preventDefault();
                                        if (currentFocus > -1) {
                                          /*and simulate a click on the "active" item:*/
                                          if (x) x[currentFocus].click();
                                        }
                                      }
                                  });
                                  function addActive(x) {
                                    /*a function to classify an item as "active":*/
                                    if (!x) return false;
                                    /*start by removing the "active" class on all items:*/
                                    removeActive(x);
                                    if (currentFocus >= x.length) currentFocus = 0;
                                    if (currentFocus < 0) currentFocus = (x.length - 1);
                                    /*add class "autocomplete-active":*/
                                    x[currentFocus].classList.add("autocomplete-active");
                                  }
                                  function removeActive(x) {
                                    /*a function to remove the "active" class from all autocomplete items:*/
                                    for (var i = 0; i < x.length; i++) {
                                      x[i].classList.remove("autocomplete-active");
                                    }
                                  }
                                  function closeAllLists(elmnt) {
                                    /*close all autocomplete lists in the document,
                                    except the one passed as an argument:*/
                                    var x = document.getElementsByClassName("autocomplete-items");
                                    for (var i = 0; i < x.length; i++) {
                                      if (elmnt != x[i] && elmnt != inp) {
                                        x[i].parentNode.removeChild(x[i]);
                                      }
                                    }
                                  }
                                  /*execute a function when someone clicks in the document:*/
                                  document.addEventListener("click", function (e) {
                                      closeAllLists(e.target);
                                      });
                                }

                                /*An array containing all the country names in the world:*/
                                var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Wakanda","Yemen","Zambia","Zimbabwe"];

                                /*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
                                autocomplete(document.getElementById("myInput"), countries);
                            </script>
                            
<!--
                            <ul class="nav navbar-nav">
                                <li><a href="#"><i class="fa fa-user"></i> Account</a></li>
                                <li><a href="#"><i class="fa fa-star"></i> Wishlist</a></li>
                                <li><a href="<?php echo HOME . 'checkout.php' ?>"><i class="fa fa-crosshairs"></i> Checkout</a></li>
                                <li><a href="<?php echo HOME . 'cart.php' ?>"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                                <li><a href="<?php echo HOME . 'login.php' ?>"><i class="fa fa-lock"></i> Login</a></li>
                            </ul>
-->
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
<!--header-bottom-->
        <div class="header-bottom pd_bt0">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
<!--
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="mainmenu pull-left">
                            <ul class="nav navbar-nav collapse navbar-collapse">
                                
                                
                            </ul>
                        </div>
-->
                    </div>
                </div>
            </div>
        </div>
        <!--/header-bottom-->
        <div class="header-bottom pad_tp0">
            <nav id="navbar" class="navbar marg_bt0 navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo HOME . 'index.php' ?>">Home</a>
                   
                </div>
                
                <div class="collapse navbar-collapse js-navbar-collapse">
                    <div class="container"></div>
                    <ul class="nav navbar-nav">
                        <li class="dropdown mega-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Personalised Gifts <span class="caret"></span></a>				
                            <ul class="dropdown-menu mega-dropdown-menu">
                                <li class="col-sm-3">
                                <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Features</li>
                                        <li><a href="#">Auto Carousel</a></li>
                                        <li><a href="#">Carousel Control</a></li>
                                        <li><a href="#">Left & Right Navigation</a></li>
                                        <li><a href="#">Four Columns Grid</a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Fonts</li>
                                        <li><a href="#">Glyphicon</a></li>
                                        <li><a href="#">Google Fonts</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Plus</li>
                                        <li><a href="#">Navbar Inverse</a></li>
                                        <li><a href="#">Pull Right Elements</a></li>
                                        <li><a href="#">Coloured Headers</a></li>                            
                                        <li><a href="#">Primary Buttons & Default</a></li>							
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                            </ul>				
                        </li>
                        <li class="dropdown mega-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Corporate Gifts <span class="caret"></span></a>				
                            <ul class="dropdown-menu mega-dropdown-menu">
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Features</li>
                                        <li><a href="#">Auto Carousel</a></li>
                                        <li><a href="#">Carousel Control</a></li>
                                        <li><a href="#">Left & Right Navigation</a></li>
                                        <li><a href="#">Four Columns Grid</a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Fonts</li>
                                        <li><a href="#">Glyphicon</a></li>
                                        <li><a href="#">Google Fonts</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Plus</li>
                                        <li><a href="#">Navbar Inverse</a></li>
                                        <li><a href="#">Pull Right Elements</a></li>
                                        <li><a href="#">Coloured Headers</a></li>                            
                                        <li><a href="#">Primary Buttons & Default</a></li>							
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                <ul>
                                        <li class="dropdown-header">Much more</li>
                                        <li><a href="#">Easy to Customize</a></li>
                                        <li><a href="#">Calls to action</a></li>
                                        <li><a href="#">Custom Fonts</a></li>
                                        <li><a href="#">Slide down on Hover</a></li>                         
                                    </ul>
                                </li>
                            </ul>				
                        </li>
                        <li><a href="#">Store locator</a></li>
                        <li><a class="navbar-brand" href="#">Home</a></li>
                        <li class="dropdown"><a class="navbar-brand" href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo HOME . 'shop.php' ?>">Products</a></li>
                                        <li><a href="<?php echo HOME . 'product-details.php' ?>">Product Details</a></li>
                                        <li><a href="<?php echo HOME . 'checkout.php' ?>">Checkout</a></li>
                                        <li><a href="<?php echo HOME . 'cart.php' ?>">Cart</a></li>
                                        <li><a href="<?php echo HOME . 'login.php' ?>">Login</a></li>
                                    </ul>
                        </li>
                        <li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="<?php echo HOME . 'blog.php' ?>">Blog List</a></li>
                                        <li><a href="<?php echo HOME . 'blog-single.php' ?>">Blog Single</a></li>
                                    </ul>
                                </li>
                        <li><a href="<?php echo HOME . '404.php' ?>">404</a></li>
                        <li><a href="<?php echo HOME . 'contact-us.php' ?>">Contact</a></li>
                        
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                    <li><p class="navbar-text">Already have an account?</p></li>
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
                        <ul id="login-dp" class="dropdown-menu">
                            <li>
                                <div class="row">
                                        <div class="col-md-12">
                                            Login via
                                            <div class="social-buttons">
                                                <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                                <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                                            </div>
                                            or
                                            <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                                    <div class="form-group">
                                                        <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                        <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                                        <div class="help-block text-right"><a href="">Forgot the password ?</a></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                        <input type="checkbox"> keep me logged-in
                                                        </label>
                                                    </div>
                                            </form>
                                        </div>
                                        <div class="bottom text-center">
                                            New here ? <a href="#"><b>Join Us</b></a>
                                        </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
                </div><!-- /.nav-collapse -->
            </div>
            </nav>
            <script>
                window.onscroll = function() {myFunction()};

                var navbar = document.getElementById("navbar");
                var sticky = navbar.offsetTop;

                function myFunction() {
                  if (window.pageYOffset >= sticky) {
                    navbar.classList.add("sticky")
                  } else {
                    navbar.classList.remove("sticky");
                  }
                }
            </script>
        </div>  
    </header><!--/header-->
        <!-- Navbar End-->
<?php
}
?>